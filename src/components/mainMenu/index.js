import React from 'react'
import { Link } from "react-router-dom"

class MainMenu extends React.Component {
    render() {
        return (
            <div className="d-flex flex-column justify-content-around h-100">
                <Link className="btn btn-primary my-4" id="create-poll-btn" to="/polls">Create a Fist To Five Poll</Link>
                <Link className="btn btn-primary my-4" to="/info" id="info-btn">What is a Fist to Five Poll?</Link>
                <a className="btn btn-primary my-4" href="https://gitlab.com/learning-loop" id="join-btn">Join this Open Source Project</a>
            </div>
        )
    }
}

export default MainMenu