
import React from 'react';
import './voteButton.css'
let VoteButton = props => {
    return (
        <label htmlFor={`vote_${props.voteNumber}`} className={`btn btn-primary fof-button ${props.active}`}>
            <input className='radio-button' id={`vote_${props.voteNumber}`} type="radio" value={props.voteNumber} onClick={props.handleChange} onChange={props.handleChange} name='vote' />{props.voteNumber}
        </label>
    )
}

export default VoteButton