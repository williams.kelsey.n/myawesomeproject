
import React from 'react';
let VoteResults = props => {
    return (
        <div id={`vote_results_${props.voteNumber}`}>
            <h4>{props.results}</h4>
        </div>
    )
}

export default VoteResults