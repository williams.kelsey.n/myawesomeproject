import React from 'react';
import { shallow } from 'enzyme';
import VoteResults from '.';


describe('', () => {
    let wrapper, props
    beforeEach(() => {
        props = {
            results: 12312,
            voteNumber: 1
        }
        wrapper = shallow(<VoteResults {...props} />);
    })

    it('renders without crashing', () => {
        expect(wrapper.find('#vote_results_1')).toHaveLength(1)
    })

    it('THEN displays the result of the vote', () => {
        expect(wrapper.find('#vote_results_1').children('h4').text() * 1).toEqual(props.results)
    })
})