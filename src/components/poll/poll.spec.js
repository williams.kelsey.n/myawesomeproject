import React from 'react';
import { shallow } from 'enzyme';
import Poll from '.';
import VoteButton from '../voteButton'

describe('Given A voter opens the voting selection screen', () => {
    let wrapper
    beforeEach(() => {
        wrapper = shallow(<Poll />);
    })

    describe('WHEN The screen opens', () => {
        it('THEN it renders without crashing', () => {
            expect(wrapper.find('.App')).toHaveLength(1)
        });

        it('THEN there are five buttons available for selection', () => {
            expect(wrapper.state('votes')).toEqual([5, 4, 3, 2, 1])
            expect(wrapper.find(VoteButton)).toHaveLength(5)
        })

        it('THEN none of currently selected', () => {
            expect(wrapper.state().selectedVote).toBeNull

        })
    })

    describe('WHEN The voter selects a button', () => {
        it('THEN the button visually indicates that it is selected and the Boolean status is set to True', () => {
            expect(wrapper.find(VoteButton).first().prop('active')).toEqual('')
            wrapper.find(VoteButton).first().prop('handleChange')({ target: { value: '5' } })
            expect(wrapper.state().selectedVote).toEqual('5')
            expect(wrapper.find(VoteButton).first().prop('active')).toEqual('active')
        })
    })
})