import React, { Component } from 'react';
import VoteButton from '../voteButton'

class Poll extends Component {
    constructor(props) {
        super(props)


        this.state = {
            votes: [5, 4, 3, 2, 1],
            selectedVote: ''
        }
    }

    selectVoteOption = (e) => {
        this.setState({ selectedVote: e.target.value })
        return
    }

    isActive = (vote) => this.state.selectedVote === vote + '' ? 'active' : ''

    render() {
        return (
            <div className="App d-flex flex-column justify-content-center align-items-center">
                {this.state.votes.map(vote => <VoteButton key={vote} active={this.isActive(vote)} handleChange={this.selectVoteOption} voteNumber={vote} />)}
            </div>
        );
    }
}

export default Poll;
