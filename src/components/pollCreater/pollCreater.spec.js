import React from 'react';
import { shallow } from 'enzyme';
import PollCreater from '.'
import { wrap } from 'module';


describe('', () => {
    let wrapper
    beforeEach(() => {

        wrapper = shallow(<PollCreater />);

    })
    it('should render with three elements a title, text area and button', () => {
        expect(wrapper.find('h3#poll-creation-title')).toHaveLength(1)
        expect(wrapper.find('textarea#poll-question')).toHaveLength(1)
        expect(wrapper.find('button#submit-poll-btn')).toHaveLength(1)
    })

    it('should have classs for the textarea', () => {
        expect(wrapper.find('textarea#poll-question').props().className).toEqual("border border-primary")
    })

    it('should have a set row length and column length for the text area', () => {
        expect(wrapper.find('textarea#poll-question').props().rows).toEqual("5")
        expect(wrapper.find('textarea#poll-question').props().cols).toEqual("30")

    })

    it('should have limit of 255 characters in the text area', () => {
        expect(wrapper.find('textarea#poll-question').props().maxLength).toEqual("255")
    })

    it('should have no default value in the text area', () => {
        expect(wrapper.find('textarea#poll-question').text()).toEqual('')
    })


})