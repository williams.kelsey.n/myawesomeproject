import React from 'react'

class PollCreater extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            defaultTextArea: ''
        }
    }
    render() {
        return (<div className="d-flex flex-column justify-content-start">
            <h3 className="text-center my-3" id="poll-creation-title">Enter a Poll Title</h3>
            <div className="input-group my-3">
                <textarea className={"border border-primary"} maxLength="255" id="poll-question" rows="5" cols="30" defaultValue={this.state.defaultTextArea}></textarea>
            </div>
            <button id="submit-poll-btn" className="btn btn-primary my-3" type="button">Create Poll</button>
        </div >)
    }
}
export default PollCreater