import React from 'react';
import ErrorBanner from '../banners/error'
let errorMessage = 'Something went wrong. Please refresh page'
class ErrorComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hasError: false
        }
    }
    componentDidCatch(error, errorInfo) {
        this.setState({ hasError: true })
        return
    }
    render() {
        return (this.state.hasError) ? <div><ErrorBanner message={errorMessage} /> {this.props.children}</div> : this.props.children
    }
}

export default ErrorComponent