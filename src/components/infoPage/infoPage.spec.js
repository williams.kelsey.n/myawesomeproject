import React from 'react';
import { shallow } from 'enzyme';
import InfoPage from '.'
import { wrap } from 'module';


describe('', () => {
    let wrapper
    beforeEach(() => {

        wrapper = shallow(<InfoPage />);

    })
    it('should render with an element containing id', () => {
        expect(wrapper.find('div')).toHaveLength(1)
    })
})