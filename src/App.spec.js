import React from 'react';
import { shallow, mount } from 'enzyme';
import App from './App';
import InfoPage from './components/infoPage'
import Poll from './components/poll'
import MainMenu from './components/mainMenu'
import PollCreater from './components/pollCreater'
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom"
import { MemoryRouter } from 'react-router';

import ErrorComponent from './components/errorComponent'

describe('Given A voter opens the voting selection screen', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<App />);
  })

  describe('WHEN The screen opens', () => {
    it('THEN it renders without crashing', () => {
      expect(wrapper.find('.App')).toHaveLength(1)
      expect(wrapper.find(ErrorComponent)).toHaveLength(1)
      expect(wrapper.find(Router)).toHaveLength(1)
      expect(wrapper.find(Switch)).toHaveLength(1)
      expect(wrapper.find(Route)).toHaveLength(4)
    });

    describe('WHEN a poll url is entered', () => {
      it('THEN render poll screen', () => {
        const mountedWrapper = mount(<MemoryRouter initialEntries={['/123124123']}><App /></MemoryRouter>);
        expect(mountedWrapper.find(Poll)).toHaveLength(1)
        expect(mountedWrapper.find(InfoPage)).toHaveLength(0)
        expect(mountedWrapper.find(MainMenu)).toHaveLength(0)
        expect(mountedWrapper.find(PollCreater)).toHaveLength(0)
        mountedWrapper.unmount()
      })
    })
    describe('WHEN a info url is entered', () => {
      it('THEN render Info Page screen', () => {
        const mountedWrapper = mount(<MemoryRouter initialEntries={['/info']}><App /></MemoryRouter>);
        expect(mountedWrapper.find(InfoPage)).toHaveLength(1)
        expect(mountedWrapper.find(Poll)).toHaveLength(0)
        expect(mountedWrapper.find(MainMenu)).toHaveLength(0)
        expect(mountedWrapper.find(PollCreater)).toHaveLength(0)
        mountedWrapper.unmount()
      })
    })
    describe('WHEN a info url is entered', () => {
      it('THEN render Info Page screen', () => {
        const mountedWrapper = mount(<MemoryRouter initialEntries={['/']}><App /></MemoryRouter>);
        expect(mountedWrapper.find(MainMenu)).toHaveLength(1)
        expect(mountedWrapper.find(InfoPage)).toHaveLength(0)
        expect(mountedWrapper.find(Poll)).toHaveLength(0)
        expect(mountedWrapper.find(PollCreater)).toHaveLength(0)
        mountedWrapper.unmount()

      })
    })

    describe('WHEN a info url is entered', () => {
      it('THEN render Info Page screen', () => {
        const mountedWrapper = mount(<MemoryRouter initialEntries={['/polls']}><App /></MemoryRouter>);
        expect(mountedWrapper.find(MainMenu)).toHaveLength(0)
        expect(mountedWrapper.find(InfoPage)).toHaveLength(0)
        expect(mountedWrapper.find(Poll)).toHaveLength(0)
        expect(mountedWrapper.find(PollCreater)).toHaveLength(1)
        mountedWrapper.unmount()

      })
    })
  })
})