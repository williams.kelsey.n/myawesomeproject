import React, { Component } from 'react';
import './App.css';
import Poll from './components/poll'
import ErrorComponent from './components/errorComponent'
import InfoPage from './components/infoPage'
import PollCreater from './components/pollCreater'
import MainMenu from './components/mainMenu'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

/**
 * vote
 * vote.voteNumber
 * vote.results
 */
class App extends Component {
  render() {

    return (
      <div className="App d-flex flex-column justify-content-center align-items-center">
        <ErrorComponent>
          <Router>
            <Switch>
              <Route path="/info" component={InfoPage} />
              <Route path="/polls" component={PollCreater} />
              <Route path="/:voteName" component={Poll} />
              <Route exact path="/" component={MainMenu} />
            </Switch>
          </Router>
        </ErrorComponent>
      </div >

    );
  }
}

export default App;
